#! /usr/bin/env sh

prefix='https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries'
glr='/usr/local/bin/gitlab-runner'
really_long_prefix="/xxxxxxx/xxxxxxx/xxxxxxx/xxxxxxx"       #  32 characters
really_long_prefix="$really_long_prefix$really_long_prefix" #  64 characters
really_long_prefix="$really_long_prefix$really_long_prefix" # 128 characters
really_long_prefix="$really_long_prefix$really_long_prefix" # 256 characters

usage() {
    echo "$0 TAG_LIST REGISTRATION_TOKEN /path/to/secret/deploy.key"
    exit 1
}

dir="$( readlink -e "$( dirname "$0" )/.." )"

taglist="$1" ; shift
token="$1" ; shift
deploykey="$1" ; shift

if [ -z "$taglist" -o -z "$token" -o -z "$deploykey" ] ; then usage ; fi

if [ "$token" '=' '--docker--' ] ; then
    mkdir -p ~/.spack
    (
      echo '---'
      echo 'config:'
      echo "  install_tree: '$really_long_prefix'"
    ) > ~/.spack/config.yaml

    curl -L --output "$glr" "$prefix/gitlab-runner-linux-amd64"
    chmod +x "$glr"
    touch /glr-ready

    mkdir -m 755 -p "$really_long_prefix"
    mkdir -p ~/.ssh

    "$glr" install --user root
    "$glr" run --user root
else
    mkdir -p "$dir/mirror"
    container="$(
        docker run --rm -d -it \
                   -v "$( readlink -e "$0" ):/rgr" \
                   -v "$dir/mirror:/mirror" \
                   --entrypoint "/bin/sh" spack/ubuntu-bionic \
                   /rgr . --docker-- .
    )"

    hn="$( docker exec "$container" hostname )"

    docker exec "$container" \
        sh -c "while [ '!' -f /glr-ready ]; do sleep 1 ; done"

    docker cp "$deploykey" "$container:/root/.ssh/id_rsa"
    docker exec "$container" chmod 600 "/root/.ssh/id_rsa"
    docker exec "$container" sh -c \
        'ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts'

    docker exec -i "$container" \
        "$glr" register \
            --non-interactive \
            --name "$hn" \
            --registration-token "$token" \
            --url https://gitlab.com \
            --executor shell \
            --tag-list "$taglist" \
            --pre-build-script '. /opt/spack/share/spack/setup-env.sh'

    trap true INT

    docker logs -f "$container"
    docker exec "$container" "$glr" unregister --name "$hn"
    docker stop "$container"
fi
