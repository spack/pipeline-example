# Tutorial: Running a Simple Pipeline on gitlab.com

This tutorial will guide you through setting up and running the minimum
components necessary to run a full binary packaging pipeline using
[Spack](https://spack.io).

Things you'll need:
 - An account on [gitlab.com](https://www.gitlab.com).
 - Git for cloning and pushing changes to your Gitlab repository.
 - GnuPG and OpenSSH for generating the necessary keys.
 - [Docker](https://www.docker.com) for running a simple Gitlab CI Runner.

### Fork this repository
 - Browse to [https://gitlab.com/spack/pipeline-example](https://gitlab.com/spack/pipeline-example)
 - Click on the "fork" text to create a fork [(Figure 1)](#figures).
 - Select a namespace to complete the fork creation process.
 - Clone your forked repository.

```bash
git clone ... && cd pipeline-example
```

### Prepare keys and configure your fork

 - Run the provided key generation script.  This script will create the deploy
   key your runner will need to push generated workloads, and the signing key it
   will need to sign the binary packages that it builds.

```bash
./scripts/generate-keys.sh
```

 - Navigate to the "repository" settings for your fork.
 - (OPTIONAL) Under "Protected Branches", "unprotect" your fork's `master`
   branch.  Alternatively, ensure that changes you make later on in this
   tutorial are pushed to a new branch.
 - Under "Deploy Keys", add a new deploy key with "Write access allowed".  Copy
   and paste the contents of the `./keys/deploy-public.key` file in the "Key"
   text area.

 - Navigate to the "CI/CD" settings for your fork.
 - Under the "Runners" section, copy the registration token shown under
   "Specific Runners" and save it.  You will need this token later
   [(Figure 2)](#figures).
 - Under the "Variables" section, add a new variable with the Key
   "SPACK_SIGNING_KEY".  For its value, copy and paste the contents of the
   `./keys/package-signing.key` file.  Click on the "Save variables" button.

### Setup and run your Gitlab CI Runner

 - In a separate shell session, run the provided gitlab runner script.  This
   script sets up and runs a Gitlab CI runner in a docker container.  This
   runner will have Spack installed, will automatically register itself to your
   project as a specific runner, and will unregister itself upon termination.
   You can terminate this runner at any time by sending the script process a
   SIGINT signal (or pressing CTRL-C in its terminal).  The required arguments
   are, in order: the list of tags to apply to the runner, the runner
   registration token that you copied and saved earlier, and the path to the
   secret part of the deploy key you generated earlier.

```bash
# example arguments
./scripts/run-gitlab-runner.sh \
    spack-demo \
    zu8j-us9zDHqrKpx-rVh \
    ./keys/deploy-secret.key
```

 - (OPTIONAL) Navigate back to the "Runners" section of the "CI/CD" settings for
   your fork.  Under "Specific Runners", you should see a new entry near the
   bottom representing the runner that you just started.

### Modify your fork and push a new commit

 - Make any modifications that you would like to your fork's working copy and
   push a new commit.  At a minimum, you should replace the stub
   `.gitlab-ci.yml` file with the `real-gitlab-ci.yml` file. This "real" file is
   meant to work as-is without any further modifications.

```bash
cp real-gitlab-ci.yml .gitlab-ci.yml
```

 - (OPTIONAL) You may wish to make additional modifications to the
   `.gitlab-ci.yml` and/or `spack.yaml` files.  If you do, please keep in mind
   the additional [notes](#notes) below.  See this
   [this](https://spack.readthedocs.io/en/latest/environments.html#spec-matrices)
   section of Spack documentation for more information on modifying the spec
   list of a Spack environment.

That's it!  Upon pushing your changes, and if everything was prepared correctly,
the Gitlab project for your fork should automatically trigger CI jobs.  Your
runner should begin to accept these jobs and execute them.  You can browse to
your project's "Pipelines" area to see the status of your triggered pipelines
[(Figure 3)](#figures).
You can also check the `./mirror` directory in your clone's working copy.  This
directory should have been automatically created when creating the runner, and
should become populated with signed binary packages as your runner produces
them.

### Notes

For brevity and simplicity, this example pipeline only demonstrates the minimum
set of functionality needed to run binary packaging pipelines with
[Spack](https://spack.io).  This tutorial was designed so that a reader
following it could run Spack pipelines with the minimum amount of effort.
Readers are encouraged to extend this example for their own purposes.  For
example, development teams working on an application could use this process to
provide up-to-date binaries for their application's dependencies on a continuous
basis, or system administrators can use it to provide users large selections of
preinstalled software packages that are optimized for their system.  If
considering using this example as a starting point for your own pipelines, it is
strongly advised that you consider the important details noted
below.  For more information, consult the
[Spack Pipelines](https://spack.readthedocs.io/en/latest/pipelines.html)
documentation.

 - This example pipeline only builds bzip2 and dependencies. Spack's CI
   capabilities can handle much larger build matrices with hundreds of
   individual package builds.  As you scale up the size of your software stacks,
   keep in mind that there is a 1 hour limit on CI jobs for projects hosted on
   [gitlab.com](https://www.gitlab.com).  Dependending on the specs of the
   systems your runners run on and the packages you'd like to build: you may
   find that some of your pipeline's jobs require more time to complete.  You
   can host your project on another Gitlab instance that allows more time, or
   deploy a Gitlab installation of your own.

 - This example does not take advantage of Spack's support for reporting build
   information to a [CDash](https://cdash.org) server.  Consider setting up a
   CDash server and configuring it to accept reports from Spack.

 - There are many important details surrounding how you set up your CI runners
   and the environments that those runners run jobs in.

   - This example prepared a runner with Spack preinstalled.  If your runners
     don't have Spack preinstalled, you'll need to install Spack in the
     beginning of every job.  See the
     [Spack Pipelines](https://spack.readthedocs.io/en/latest/pipelines.html)
     documentation for more details on how to do this.

   - This example runner stored binaries in a mirror backed by local storage.
     Spack also has support for mirrors backed by an S3 object store.  You'll
     need access to such a store-, to configure your Spack environment-, and to
     set any necessary access credentials in your runner environments- to take
     advantage of this feature.

   - In addition, ensure that your runners have the basic facilities necessary
     for pipeline operation.  For example, runners need to have network access
     to any CDash instances or any mirrors backed by remote storage.  The
     runners handling the initial concetization job need push access to whatever
     repo they push commits to (this is handled in the example by pushing to the
     same repo using a deploy key).  The environment the runners run jobs in
     must also have any necessary compilers preinstalled.

 - Extra care must be taken to ensure that runners can build specs that are
   provided to them.  In the case shown in this example, the specs in the spack
   environment are all concretized and built in the same runner environment, so
   it's a given that the runner, when given a spec to build, would have the
   necessary compilers and be of the correct `arch`.  For more complex scenarios
   with multiple runners, take care to ensure that the specs in your environment
   are sufficiently constrained-, and that your runner mappings are such-, that
   build jobs are ran by runners that can handle them.

   For example, the runner that handles the initial build generation phase will
   concretize your environment, substituting its own compilers and `arch`
   specifications if they are not constrained in your root specs.  If a job
   generated from this concretization is mapped to another runner with a
   different `arch` or set of compilers, the runner will likely fail to build
   the package.

### Figures

![Figure 1: Fork this example project.][fork]

##### Figure 1: Fork this example project.

![Figure 2: Copy the runner registration token.][runners]

##### Figure 2: Copy the runner registration token.

![Figure 3: A pipeline overview][pipeline]

##### Figure 3: A pipeline overview

[fork]: ./img/fork.png
[runners]: ./img/runners.png
[pipeline]: ./img/pipeline.png

