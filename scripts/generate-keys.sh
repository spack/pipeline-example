#! /usr/bin/env sh

gpg=gpg
which "$gpg" &> /dev/null
if [ "$?" '!=' 0 ] ; then
    gpg=gpg2
    which "$gpg" &> /dev/null
    if [ "$?" '!=' 0 ] ; then
        echo 'Cannot find a suitable version of GnuGP' >&2
        exit 1
    fi
fi

dir="$( dirname "$( dirname "$0" )" )/keys"
mkdir -p "$dir"
ssh-keygen -q -t rsa -N "" -f "$dir/deploy-key" -C 'generated deploy key'
mv "$dir/deploy-key.pub" "$dir/deploy-public.key"
mv "$dir/deploy-key" "$dir/deploy-secret.key"

tmp="$( mktemp -d )"
trap 'rm -rf "$tmp" ; exit' EXIT INT TERM QUIT

export GNUPGHOME="$tmp"

cat > "$tmp/.script" << EOF
%echo Generating a basic OpenPGP key
Key-Type: RSA
Key-Length: 2048
Subkey-Type: RSA
Subkey-Length: 2048
Name-Real: Spack Pipeline Demo
Name-Comment: Demo Key
Name-Email: key@spack.demo
Expire-Date: 0
%commit
%echo done
EOF

"$gpg" --batch                                  \
       --gen-key                                \
       --pinentry-mode=loopback --passphrase "" \
       "$tmp/.script"

(
    "$gpg" --export --armor \
 && "$gpg" --export-secret-keys --armor
) | base64 | tr -d '\n' > "$dir/package-signing.key"
